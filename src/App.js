import { Component, useState } from 'react';
import NavBar from './Components/NavBar/NavBar';
import ProductList from './Components/Products/productList';
import "./Components/index.css"
import ProductsProvider from './Components/Provider/ProductsProvider';
import Filter from './Components/Filter/Filter';
import SearchBar from './Common/Search/Search';



const App = (props) => {

    return ( 
        <ProductsProvider>

        <div className="container" id="title">
        <NavBar />
      
        <Filter />
        <ProductList />
        </div>
        
        </ProductsProvider>
     );
}
 
export default App;


