import styles from './Select.module.css'
import Select from "react-select";

const SelectComponent = ({title, ...rest}) => {
    return (
        <div className={styles.selectcontainer}>
        <span>order by :</span>
        <Select {...rest} className={styles.select}/>
    </div>
      );
}
 
export default SelectComponent;