import { useState } from 'react';
import styles from './Search.module.css'
import { UseproductsAction } from './../../Components/Provider/ProductsProvider';

const SearchBar = ({filter}) => {
    const dispatch = UseproductsAction();
    const [value, setValue] = useState("");

    const changeHandler = (e) =>{

        dispatch({type : "Filter", selectedOption : filter});
        dispatch({type : "searchFilter", event : e});
        setValue(e.target.value);
    }
    return ( 
        <div className={styles.formControl}>
            <div>search for ...</div>
            <input
        type="text"
        placeholder="search for ..."
        onChange={changeHandler}
        value={value}
      />
        </div>
     );
}
 
export default SearchBar;