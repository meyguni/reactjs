import { useState, useRef, useEffect, } from 'react';

const UseRef = () => {

  const [value, setValue] = useState("");
  const [number, setNumber] = useState(0);

  const previousValue = useRef();
  const previousNumber = useRef();

  const InputVal = (e) => {
    setValue(e.target.value);
  }
  const ResetHandler = () => {
    setValue("");
    
  }
  useEffect( () => {
    previousValue.current = value;
  }, [value]);

  useEffect( () => {
    previousNumber.current = number;
  }, [number]);

  return ( 
    <div>
      <input type="text" onChange={InputVal} value={value} />
      <button onClick={ResetHandler}>Reset</button>
      <p>random number is {value} - previous value is {previousValue.current}</p>

      <button onClick={() => setNumber( Math.ceil( Math.random() * 100 ) ) } >Generate random Number</button>
      <p>random number is {number} - previous value is {previousNumber.current}</p>
      

    </div>
   );
}
 
export default UseRef;