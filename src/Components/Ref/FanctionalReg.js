import { useRef, useEffect } from "react";

const FanctionalRef = () => {
    const inputRef = useRef();
    useEffect( () => {
    inputRef.current.focus();
    }, []);

    return <input type="text" ref={inputRef} /> 
}
 
export default FanctionalRef;