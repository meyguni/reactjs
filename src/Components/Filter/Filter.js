import Select from "react-select";
import { UseproductsAction } from "../Provider/ProductsProvider";
import { useState } from 'react';
import styles from './Filter.module.css'
import SelectComponent from './../../Common/Select/Select';
import SearchBar from "../../Common/Search/Search";


const filterOptions = [
    { value: "", label: "All"},
    { value: "XS", label: "XS"},
    { value: "S", label: "S"},
    { value: "M", label: "M"},
    { value: "L", label: "L"},
    { value: "XL", label: "XL"},
    { value: "XXL", label: "XXL"},
];

const sortOptions = [
    { value: "highest", label: "highest" },
    { value: "lowest", label: "lowest" },
  ];
const Filter = () => {
    const dispatch = UseproductsAction();
    const [filter, setFilter] = useState();
    const [sort, setSort] = useState();


    const FilterHandler = (selectedOption) =>{
    dispatch({type : "Filter", selectedOption});
    dispatch({type : "Sort", selectedOption});
    setFilter(selectedOption);
    console.log(selectedOption.value)
    }
    const sortHandler = (selectedOption) => {
    dispatch({ type: "sort", selectedOption });
    setSort(selectedOption);
    console.log(selectedOption);
    };

    return ( 
       <div className={styles.filter}>
        <SearchBar filter={filter}/>
        Filter products based on :
        <SelectComponent title="filter by size" options={filterOptions} onChange={FilterHandler} value={filter}/>
        <SelectComponent title="Sort by price" options={sortOptions} onChange={sortHandler} value={sort}/>
        </div> 
        );
}
 
export default Filter;