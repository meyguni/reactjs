import React, { PureComponent } from 'react';
import MemoCom from './MemoCom';
import PureCop from './PureCop';
import RegCom from './RegCom';

class ParentCom extends PureComponent {
    state = {
        name : "sepide"
    }
    componentDidMount() {
        setInterval( () =>{
         this.setState( {name : "sepide"} );
        }, 1000)
    }
    render() { 
        console.log("parent copm-----");
        return <div>
            <RegCom name={this.state.name}/>
            <PureCop name={this.state.name}/>
            <MemoCom name={this.state.name}/>
        </div>;
    }
}
 
export default ParentCom;