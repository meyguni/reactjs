import { useState } from "react";

 
const HookHandler = () => {
     const [count, setCount] = useState(0);

     const addoneHandler = () => {
         setCount(count + 1);
     }
     const addtwoHandler = () => {
        setCount(count + 2);
    }
    const addfiveHandler = () => {
        setCount(prev => prev + 5);
    }
     return ( 
        <div className="APP2"> 
        <p>count - {count}</p>
        <button onClick={addoneHandler}>count</button>
        <button onClick={addtwoHandler}>count2</button>
        <button onClick={addfiveHandler}>count5</button>
        </div>

      );
 }
  
 export default HookHandler;