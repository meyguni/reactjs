import styles from './Product.module.css';
import { BiTrashAlt } from "react-icons/bi";

const Product = (props) => {
    
    return ( 
        
        <div className={styles.Product}>
            <p>product Name : {props.product.title} </p>
            <p>product Price : {props.product.price} </p>
            <span className={styles.inc}>{props.product.quantity}</span>
            {/* <input type="text" onChange={props.onChange} value={props.product.title} /> */}
            <button className={`
            ${styles.inc} 
            ${props.product.quantity === 1 && styles.remove}
            `} 
            onClick={props.quantityminus}>
                 {props.product.quantity > 1 ? "-" : <BiTrashAlt/>}
            </button>
            <button className={styles.inc} onClick={props.quantityPlus}> + </button>
            <button className={styles.button} onClick={props.onDelete}>Delete</button>
            {props.children}
        </div>
        


     );
}
 
export default Product;