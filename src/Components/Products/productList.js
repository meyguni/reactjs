import React from "react";
import { useProducts, UseproductsAction } from "../Provider/ProductsProvider";
import Product from "./Product";

const ProductList = (props) => {
    
    const product = useProducts();
    const dispatch = UseproductsAction();
    
    const renderProduct = () => {
        
        if(product.length === 0) return <div>there is no product</div>

        return product.map((product) => {
          return (
           <Product 
            product={product}   
            onDelete={() => dispatch({type : "Remove", id : product.id})} 
            quantityPlus={() => dispatch({type : "Increment", id : product.id})}
            quantityminus={() => dispatch({type : "Decrement", id : product.id})}
            onChange={(e) => dispatch({type : "Edit", id : product.id, event : e})}
          />
           );
        });
    };
           
    return (
        <div>
        {!product.length && <div>there is no Product in cart</div>  }
        {renderProduct()}
        </div>
    );
}
 
export default ProductList;

// class ProductList extends Component {
    
    
//      renderProduct = () => {
//         if(product.length === 0)
//         return <div>there is no product</div>

//         return (  
//             <div className="body">
            
//             {product.map((product) => {
//                return (
//                    <Product 
//                    product={product}   
//                    onDelete={() => onDelete(product.id)} 
//                    quantityPlus={() => quantityPlus(product.id)}
//                    quantityminus={() => quantityminus(product.id)}
//                    onChange={(e) => onChange(e, product.id)}
//                    />
//                )
//            })}
           
      
//             </div>
//         );
//      }
//     render() { 

//         return (
//             <div>
//             {!product.length && <div>there is no Product in cart</div>  }
//             {this.renderProduct()}
//             </div>
//         );
//     }
// }
 
// export default ProductList;