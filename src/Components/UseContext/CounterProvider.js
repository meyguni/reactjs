import React from 'react';
import { useContext, useState, useReducer } from 'react';



 const CounterCountext = React.createContext();
 const CounterCountextDispatcher = React.createContext();

 const initialstate = 0;
 const reduser = (state, Action) => {
   switch (Action.type) {
       case "Add" :
           return state + Action.value;
       case "Decrement" :
           return state - Action.value; 
       case "reset" :
           return initialstate ;  
 
       default :
           return state ;         
   }
 }

const CounterProvider = ({children}) => {
    const [count, dispatch] = useReducer(reduser, initialstate);
    return ( 
        <div>
         <CounterCountext.Provider value={count}>
          <CounterCountextDispatcher.Provider value={dispatch}>
           {children}
          </CounterCountextDispatcher.Provider>
         </CounterCountext.Provider>
         
        </div>
     );
}
 
export default CounterProvider;
export const UseCount = () => useContext(CounterCountext);
export const UseAction = () => {

   return useContext(CounterCountextDispatcher);

//     const Addone = () => {
//         setCount( (prevcount) => prevcount + 1  );
//     }
//     const AddFive = () => {
//         setCount( (prevcount) => prevcount + 5 );
//     }
//     const Decrement = () => {
//         setCount( (prevcount) => prevcount - 1 );
//     }
//  return{ Addone, AddFive, Decrement}
}    
