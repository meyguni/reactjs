
import { UseCount, UseAction } from "./CounterProvider";

const OneCount = () => {
    const count = UseCount();
    const dispatch = UseAction();

    return ( 
        <div>
            <p>this is - {count}</p>
            <button onClick={() => dispatch( {type : "Add" , value : 1} )}>Add one</button>
            <button onClick={() => dispatch( {type : "reset" } )}>reset</button>
            <button onClick={() => dispatch( {type : "Decrement" , value : 1} )}>Decrement</button>
        </div>
     );
}
 
export default OneCount;