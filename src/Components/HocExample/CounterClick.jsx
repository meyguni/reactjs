import WithCounter from "../Hoc/WithCount";


const CounterClick = ( {count, incrementCount, name} ) => {
   
    return ( 
    <div>
        <h2 onClick={incrementCount}>clicked {count} times</h2>
    </div> 
    );
};
 
export default WithCounter(CounterClick, 5);

