import WithCounter from "../Hoc/WithCount";



const CounterHover = ({count, incrementCount, name}) => {

    return ( 
    <div>
        <h2 onMouseOver={incrementCount}>clicked {count} times</h2>
    </div> 
    );
}
 
export default WithCounter(CounterHover, 5);