import React from 'react';
import { useContext, useState, useReducer } from 'react';
import { productsData } from '../../db/products'
import _ from "lodash";


 const ProductsContext = React.createContext();
 const ProductsContextDispatcher = React.createContext();


const reduser = (state, Action) => {
    switch (Action.type) {
        case "Increment" :
        const Apdateproduct = [...state];
        const Productplus = Apdateproduct.find((p) => p.id === Action.id);
        Productplus.quantity++
            return Apdateproduct;

        case "Decrement" :
        const Apdateproduct2 = [...state];
        const Productminus = Apdateproduct2.find((p) => p.id === Action.id);
        if (Productminus.quantity === 1){
        const delethandler = Apdateproduct2.filter((p) => p.id !== Action.id);
           return delethandler;
        }else {
          Productminus.quantity--
          return Apdateproduct2;
        }

        case "Edit" :
            const Apdateproduct3 = [...state];
            const Productchange = Apdateproduct3.find((p) => p.id === Action.id);
            Productchange.title = Action.event.target.value;
            return Apdateproduct3;
 
        case "Remove" :
            const delethandler = state.filter((p) => p.id !== Action.id);
            return delethandler ;
        case "Filter" :
          const value = Action.selectedOption.value;
            if (value === "") {
              return productsData;  
            }else {
                const ApdatedProducts = productsData.filter(
                (p) => p.availableSizes.indexOf(value) >= 0  
                );
              return ApdatedProducts;
            }
        case "sort": {
              const value = Action.selectedOption.value;
              const products = [...state];
              if (value === "lowest") {
                return _.orderBy(products, ["price"], ["asc"]);
              } else {
                return _.orderBy(products, ["price"], ["desc"]);
              }
            }
        case "searchFilter" : {
                const value = Action.event.target.value;
                if (value === "") {
                  return state;  
                }else {
                    const ApdatedProducts = state.filter(
                    (p) => p.title.toLowerCase().includes(value.toLowerCase())
                    );
                  return ApdatedProducts;
                }    
              }

        default :
            return state ;         
    }
  } 


const ProductsProvider = ({children}) => {
    const [product, dispatch] = useReducer(reduser, productsData);
    return ( 
        <div>
         <ProductsContext.Provider value={product}>
          <ProductsContextDispatcher.Provider value={dispatch}>
           {children}
          </ProductsContextDispatcher.Provider>
         </ProductsContext.Provider>
         
        </div>
     );
}
 
export default ProductsProvider;

export const useProducts = () => useContext(ProductsContext);

export const UseproductsAction = () =>{

    return useContext(ProductsContextDispatcher);
    // const product = useContext(ProductsContext);

    // const clickHandler = (id) => {
    //     const delethandler = product.filter((p) => p.id !== id);
    //     setProduct(delethandler)
    //    }
    // const plusHandler = (id) => {
    //     const Apdateproduct = [...product];
    //     const Productplus = Apdateproduct.find((p) => p.id === id);
    //     Productplus.quantity++
    //     setProduct(Apdateproduct);
    //    }
    // const minusHandler = (id) => {
    //     const Apdateproduct = [...product];
    //     const Productminus = Apdateproduct.find((p) => p.id === id);
    //     if (Productminus.quantity === 1){
    //     const delethandler = Apdateproduct.filter((p) => p.id !== id);
    //     setProduct(delethandler)
    //     }else{
    //       Productminus.quantity--
    //       setProduct(Apdateproduct);
    //     }
    //    }
    // const ChangeHandler = (e, id) => {
    //      console.log(e.target.value);
    //      const Apdateproduct = [...product];
    //      const Productchange = Apdateproduct.find((p) => p.id === id);
    //      Productchange.title = e.target.value;
    //      setProduct(Apdateproduct);
    //    }
    // return {clickHandler, plusHandler, minusHandler, ChangeHandler}
} 

