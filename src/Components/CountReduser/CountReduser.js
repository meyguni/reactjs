import { useReducer } from 'react';

const initialstate = 0;
const reduser = (state, Action) => {
  switch (Action.type) {
      case "Add" :
          return state + Action.value;
      case "Decrement" :
          return state - Action.value; 
      case "reset" :
          return initialstate ;  

      default :
          return state ;         
  }
}

const CountReduser = () => {

    const [count, dispatch] = useReducer(reduser, initialstate);
    const [counttwo, dispatchtwo] = useReducer(reduser, initialstate);


    return (  
        <div>
            <p>this is {count}</p>
            
           <div>
            <button onClick={() => dispatch( {type : "Add" , value : 1} )}>Add one</button>
            <button onClick={() => dispatch( {type : "Add" , value : 5} )}>Add Five</button>
            <button onClick={() => dispatch( {type : "Decrement" , value : 1} )}>Decrement</button>
           </div>

            <button onClick={() => dispatch( {type : "reset"} )}>reset</button>

            <p>this is {counttwo}</p>
            
           <div>
            <button onClick={() => dispatchtwo( {type : "Add" , value : 1} )}>Add one</button>
            <button onClick={() => dispatchtwo( {type : "Add" , value : 5} )}>Add Five</button>
            <button onClick={() => dispatchtwo( {type : "Decrement" , value : 1} )}>Decrement</button>
           </div>

            <button onClick={() => dispatchtwo( {type : "reset"} )}>reset</button>
        </div>
    );
}
 
export default CountReduser;