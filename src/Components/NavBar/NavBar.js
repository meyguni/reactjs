import { useProducts } from "../Provider/ProductsProvider";
import Styles from "./Navbar.module.css"

const NavBar = () => {
    const product = useProducts();
    const TotalItems = product.filter( (p) => p.quantity > 0 ).length;

    return <header className={Styles.navbar}> 
    <h2>Bahar shopping</h2>
    <span>{TotalItems}</span>
     </header>;
}
 
export default NavBar;