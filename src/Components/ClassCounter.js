import React, { Component } from 'react';
class ClassCounter extends Component {
    state = { 
        name : "",
        count : 0 ,
     }
    componentDidMount() {
    document.title = `clicked : ${this.state.count} times`
    }
    componentDidUpdate(prevState, prevProps) {
    if (prevState.count !== this.state.count){
         document.title = `clicked : ${this.state.count} times`

    }
    
    }
    render() { 
        return ( 
            <div>
            <p>count - {this.state.count} - name - {this.state.name}</p>
            <input type="text" onChange={(e) => this.setState({ name : e.target.value})}/>
            <button onClick={() => this.setState({count : this.state.count + 1})}>count</button>
            </div>
         );
    }
}
 
export default ClassCounter;