import { useState } from "react";


const HookObject = () => {
    const [user ,setUser] = useState({firstname:"", lastname:""})
    const HookHandler = (e) => {
     setUser( { ...user, firstname:e.target.value} );
    }

    const HooklastHandler = (e) => {
     setUser( { ...user,
         lastname:e.target.value} );
    }    
    return ( 
        <div>
            <input type="text" value={user.firstname} onChange={HookHandler}></input>
            <input type="text" value={user.lastname} onChange={HooklastHandler}></input>
            <p>my name is - {user.firstname}</p>
            <p>my name is - {user.lastname}</p>
        </div>
     );
}
 
export default HookObject;