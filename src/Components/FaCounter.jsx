import { useEffect, useState } from 'react';

const FaCounter = () => {
    const [name, setName] = useState("");
    const [count, setCount] = useState(0);
    useEffect(() => {
        document.title = `clicked ${count} time`
    }, [count])
    return ( 
        <div>
        <input type="text" onChange={(e) => setName( e.target.value )}/>
        <button onClick={() => setCount(count + 1)} value={name}>
            count : {count}
            </button>
        </div>
     );
}
 
export default FaCounter;