import { useState } from "react";


const WithCounter = (WrapperComponent, incrementValue) => {
    const UpDateComponent = (props) => {
        const [count, setCount] = useState(0);
        const incrementCount = () => {
        setCount(count + incrementValue);
        };

          
        
    return (
    <WrapperComponent count={count} incrementCount={incrementCount} {...props}/>
    );
    };
    return UpDateComponent;
};

export default WithCounter;

